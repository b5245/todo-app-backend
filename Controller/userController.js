const User = require(`../Model/User`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth`)

module.exports = {

    register: (req, res) => {
        User.findOne({ email: req.body.email }).then(result => {

            const newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10)
            })

            newUser.save().then((succ, err) => {
                if (err) {
                    res.send(err)
                }
                else {
                    res.send(succ)
                }
            })


        })

    },

    getUsers: (req, res) => {
        User.find().then(result => {
            res.send(result)
        })
    },

    login: (req, res) => {
        User.findOne({ email: req.body.email }).then(result => {
            if (result == null) {
                res.send(false)
            }
            else {
                const passMatch = bcrypt.compareSync(req.body.password, result.password)
                if (passMatch) {
                    res.send({ access: auth.createAccessToken(result) })
                }
                else {
                    res.send(false)
                }
            }
        })
    },

    checkEmail: (req, res) => {
        User.findOne({ email: req.body.email }).then(result => {
            if (result == null) {
                res.send(false)
            }
            else {
                res.send(true)
            }
        })
    },

    details: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        User.findById(userData.id).then(result => {
            res.send(result)
        })
    }
}