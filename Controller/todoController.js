const Todo = require(`../Model/Todo`)
const bcrypt = require(`bcrypt`)
const auth = require(`../auth`)

module.exports = {

    createTask: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        const newTodo = new Todo({
            task: req.body.task,
            userId: userData.id
        })

        newTodo.save().then((succ, err) => {
            if (err) {
                res.send(err)
            }
            else {
                res.send(succ)
            }
        })
    },

    getMyTask: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        Todo.find({ userId: userData.id }).then(result => {
            res.send(result)
        })
    },

    prioritize: (req, res) => {
        Todo.findById(req.body.id).then(result => {

            result.updateOne({
                $set: {
                    priority: req.body.priority,
                    archived: false
                }
            }).then(result => { })

            result.save().then((succ, err) => {
                if (err) {
                    res.send(false)
                }
                else {
                    res.send(true)
                }
            })
        })
    },

    archive: (req, res) => {
        Todo.findById(req.body.id).then(result => {
            result.updateOne({
                $set: {
                    archived: req.body.archived,
                    priority: false
                }
            }).then(result => { })

            result.save().then((succ, err) => {
                if (err) {
                    res.send(false)
                }
                else {
                    res.send(true)
                }
            })
        })
    },

    delete: (req, res) => {
        Todo.findByIdAndDelete(req.body.id).then((succ, err) => {
            if (err) {
                res.send(err)
            }
            else {
                res.send(succ)
            }
        })
    },
    getPriority: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        Todo.aggregate([
            {
                $match: {
                    userId: userData.id,
                    priority: true
                }
            }
        ]).then(result => {
            res.send(result)
        })
    },

    getArchived: (req, res) => {
        const userData = auth.decode(req.headers.authorization)

        Todo.aggregate([
            {
                $match: {
                    userId: userData.id,
                    archived: true
                }
            }
        ]).then(result => {
            res.send(result)
        })
    },

    update: (req, res) => {
        Todo.findByIdAndUpdate({ _id: req.body.id }, {
            $set: {
                task: req.body.task
            }
        }).then(result => {
            res.send(result)
        })
    }
}