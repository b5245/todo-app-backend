const express = require(`express`)
const router = express.Router()
const todoController = require(`../Controller/todoController`)
const auth = require(`../auth`)

router.post('/create', auth.verify, todoController.createTask)

router.get('/getmytask', auth.verify, todoController.getMyTask)

router.patch('/prioritize', todoController.prioritize)

router.patch('/archive', todoController.archive)

router.delete('/delete', todoController.delete)

router.get('/getpriority', auth.verify, todoController.getPriority)

router.get('/getarchived', auth.verify, todoController.getArchived)

router.patch('/update', todoController.update)



module.exports = router;