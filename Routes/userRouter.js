const express = require(`express`)
const router = express.Router()
const userController = require(`../Controller/userController`)
const auth = require(`../auth`)

router.post('/register', userController.register)

router.get('/', userController.getUsers)

router.post('/login', userController.login)

router.post('/checkemail', userController.checkEmail)

router.get('/details', auth.verify, userController.details)


module.exports = router;