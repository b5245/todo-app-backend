const mongoose = require(`mongoose`)

const todoSchema = mongoose.Schema({
    task: {
        type: String,
        required: true
    },
    priority: {
        type: Boolean,
        default: false
    },
    archived: {
        type: Boolean,
        default: false
    },

    userId: {
        type: String,
        required: true
    },
    dateCreated: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model(`Todos`, todoSchema)