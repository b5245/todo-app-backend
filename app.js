const express = require(`express`)
const app = express()
const mongoose = require(`mongoose`)
const port = 4000
const cors = require('cors')



app.use(cors({
    allowedHeaders: '*',
    allowMethods: '*',
    origin: '*'
}));
app.use(express.json())
app.use(express.urlencoded({ extended: true }))


const userRoute = require('./Routes/userRouter')
app.use('/users', userRoute)
const todoRoute = require('./Routes/todoRouter')
app.use('/todo', todoRoute)

mongoose.connect(`mongodb+srv://admin:admin@wdc028-course-booking.mgfy3.mongodb.net/todo-app?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => console.log(`Connected to MongoDB`))

app.listen(process.env.PORT || port, () => { console.log(`API now online on port ${process.env.PORT || port}`) })